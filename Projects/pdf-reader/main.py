import PyPDF2


def main():
    # file obj
    pdfFileObj = open('data_files/terraform-associate-pdf.pdf', 'rb')

    # creating a pdf reader object
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

    # printing number of pages in pdf file
    print(pdfReader.numPages)

    for page in range(pdfReader.numPages):
    #for page in range(1):
        # For each page split text per number
        page_data = pdfReader.getPage(page)
        # extract text
        text = page_data.extractText()
        split_text = text.split("Question")
        process_text(split_text)

    # closing the pdf file object
    pdfFileObj.close()


def process_text(data):
    for i in data:
        print(i.strip())


if __name__ == "__main__":
    main()